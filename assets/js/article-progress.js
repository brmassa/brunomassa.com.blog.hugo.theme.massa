// Get all article tags on the page
const articles = document.querySelectorAll('article');

// Function to update the progress bar based on scroll position
function updateProgressBar() {
  articles.forEach((article) => {
    const progressBar = article.querySelector('.progress-bar');
    const progressBarFill = article.querySelector('.progress-bar-fill');

    const articleTop = article.getBoundingClientRect().top;
    const articleHeight = article.offsetHeight;

    let scrollPercentage;

    if (articleTop <= 0 && Math.abs(articleTop) <= articleHeight) {
      scrollPercentage = (Math.abs(articleTop) / (articleHeight ) * 100);
      progressBar.style.visibility = 'visible';
      progressBarFill.style.width = `${scrollPercentage}%`;
    } else {
      scrollPercentage = 0;
      progressBar.style.visibility = 'hidden';
    }
  });
}

// Attach the scroll event listener to update the progress bar
window.addEventListener('scroll', updateProgressBar);