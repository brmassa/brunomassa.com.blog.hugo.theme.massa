document.addEventListener("keydown", function (event) {
  // Check if the pressed key is "j"
  if (event.key === "j") {
    // Navigate to the next article
    navigate(1);
  }
  // Check if the pressed key is "k"
  else if (event.key === "k") {
    // Navigate to the previous article
    navigate(-1);
  }
});

function navigate(direction) {
  const articles = document.getElementsByTagName("article");
  const currentArticle = document.querySelector("article.focus");

  // Find the index of the current article in the list of articles
  const currentIndex = Array.from(articles).indexOf(currentArticle);

  // Calculate the index of the next article using modulo operator
  const nextIndex = (currentIndex + direction + articles.length) % articles.length;

  // Scroll the next article to the top of the page
  const nextArticle = articles[nextIndex];

  if (nextArticle) {
    if (currentArticle)
      currentArticle.classList.remove("focus");
    nextArticle.scrollIntoView({ behavior: "smooth", block: "start" });
    nextArticle.classList.add("focus");
  }
}
